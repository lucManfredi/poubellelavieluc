package com.example.poubellelavie.vue;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.poubellelavie.R;
import com.example.poubellelavie.bdd.AccesLocal;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infodechet);

        Bundle bundle = getIntent().getExtras();
        String act2MyValue= bundle.getString("valeurItem");

        AccesLocal bdd = new AccesLocal(this);

        TextView t1 = (TextView) findViewById(R.id.textViewInfoDechet);
        t1.setText("le "+act2MyValue+" va dans la poubelle "+bdd.getPoubelle(this, act2MyValue));

        Button buttonRetour = (Button) findViewById(R.id.buttonRetour);

        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InfoActivity.this.finish();

            }
        });


    }
}