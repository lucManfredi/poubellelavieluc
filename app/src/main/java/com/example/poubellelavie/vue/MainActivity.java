package com.example.poubellelavie.vue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.poubellelavie.R;
import com.example.poubellelavie.adapter.AdapterPersoMain;
import com.example.poubellelavie.bdd.AccesLocal;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Toast.makeText(this, "Bonjour !!!", LENGTH_SHORT).show();

        // Récupération de la liste
        ListView listeV = (ListView) findViewById(R.id.ListViewDechet);

        // Liste des valeurs affchées dans la listeView
        List<String> listeValeursDansLaListe = new ArrayList<>();
        List<Integer> listeImagesDansLaListe = new ArrayList<>();

        AdapterPersoMain adapter = new AdapterPersoMain(this, listeValeursDansLaListe, listeImagesDansLaListe);

        // lie l'adapter à la listeView
        listeV.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        AccesLocal bdd = new AccesLocal(this);
        ArrayList<String> tabDechet = new ArrayList<String>();
        //bdd.recupTypePoubelle(this);
        tabDechet = bdd.ListeDechet(this);
        for (int i = 0 ; i<tabDechet.size(); i++){
            listeValeursDansLaListe.add(tabDechet.get(i));
            switch (tabDechet.get(i)){
                case "plastique":
                    listeImagesDansLaListe.add(R.drawable.bouteille);
                    break;
                case "verre":
                    listeImagesDansLaListe.add(R.drawable.verre);
                    break;
                case "papier":
                    listeImagesDansLaListe.add(R.drawable.papier);
                    break;
                case "metal":
                    listeImagesDansLaListe.add(R.drawable.metal);
                    break;
                case "carton":
                    listeImagesDansLaListe.add(R.drawable.carton);
                    break;
                default:
                    listeImagesDansLaListe.add(R.drawable.imagenull);
                    break;
            }
        }
        adapter.notifyDataSetChanged();

        // Code pour la gestion des clics sur ls items de la liste
        listeV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Récupère la valeur de l'item à la position sur laquelle on a cliqué
                //String valeurItem = (String) parent.getItemAtPosition(position);

                String valeurItem = listeValeursDansLaListe.get(position);


                Intent InfoActivity = new Intent(MainActivity.this, InfoActivity.class);

                //Création du bundle
                Bundle bundle = new Bundle();
                bundle.putString("valeurItem", valeurItem);
                //Passage du bundle dans l'intent qui va lancer la seconde activity
                InfoActivity.putExtras(bundle);

                startActivity(InfoActivity);
            }
        });

    }

    //Génère le menu de l'activité
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //récupère l'ID du bouton du menu cliqué
        switch(item.getItemId()) {
            case R.id.menu:
                System.exit(0) ;
                break;
            case R.id.quitter:
                System.exit(0) ;
                break;
            case R.id.jeter:
                //Intent intent = new Intent(MainActivity.this, ActivityJeter.class);
                //startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }
}

