package com.example.poubellelavie.bdd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.poubellelavie.R;

// Il faudrait encapsuler les traitements liés à la BDD dans un thread spécifique
// pour ne pas bloquer le thread UI
public class SQLClient extends SQLiteOpenHelper {

    // Vous devez gérer le numéro de version de votre BDD (a un impact sur la reconstruction de la BDD par exemple)
    public static final int DATABASE_VERSION = 1;

    // Nom du fichier contenant la BDD (sqlite = fichier)
    public static final String  DATABASE_FILE = "poubelleLaVie.db";

    // Requete de creation de la bdd (exemple simplifié)
    //public static final String SQL_CREATE = "CREATE TABLE Copro ( id INT NOT NULL , plastique INT , verre INT , papier INT , metal INT , carton INT , PRIMARY KEY (id));";

    // Requete de suppression de la bdd (exemple simplifié)
    //public static final String SQL_DELETE = "DROP TABLE IF EXISTS  Copro ;";

    private Context context;

    // Constructeur permettant d'appeler le constructeur de SQLIteOpenHelper (cf. doc)
    public SQLClient(Context context){
        super (context, DATABASE_FILE, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // On créé la BDD si besoin
        db.execSQL(this.context.getString(R.string.CREATE_COPRO));
        db.execSQL(this.context.getString(R.string.INSERT_COPRO));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Si la version de la BDD change.. Ici doit être mis le code pour traiter cette situation
        // Ici : traitement violent... On supprimme et on la créé à nouveau...
        // A adapter en fonction des besoins....
        db.execSQL(this.context.getString(R.string.DELETE_COPRO));
        this.onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db,oldVersion,newVersion);
    }
}
