package com.example.poubellelavie.bdd;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.poubellelavie.vue.MainActivity;

import java.util.ArrayList;

public class AccesLocal {

    private SQLClient accesBdd;
    private SQLiteDatabase bd;

    public AccesLocal(Context context) {
        accesBdd = new SQLClient(context);
    }

    public void recupTypePoubelle(Context context){
        bd = accesBdd.getReadableDatabase();
        String[] col = {"id","papier"};
        String[] select={};
        Cursor curs = bd.query("Copro",col,"",select,null,null,null);
        if (curs.moveToFirst()){
            do{
                long coproId = curs.getLong(curs.getColumnIndexOrThrow("id"));
                int coproPapier = curs.getInt(curs.getColumnIndexOrThrow("papier"));
                Toast.makeText(context, ""+coproId+" "+coproPapier, Toast.LENGTH_LONG).show();
            }while (curs.moveToNext());
        }

        curs.close();

    }

    public ArrayList<String> ListeDechet(Context context){
        bd = accesBdd.getReadableDatabase();
        ArrayList<String> tabDechet = new ArrayList<String>();

        String selectQuery = "select * from Copro";
        try (Cursor curs = bd.rawQuery(selectQuery, null)) {

            if (curs.moveToFirst()) {

                int i = curs.getColumnCount();
                int j = 1;
                while(j<i){
                    tabDechet.add(curs.getColumnName(j));
                    j++;
                }
            }
        }

        return tabDechet;
    }
    
    public String getPoubelle(Context context, String dechet){
        bd = accesBdd.getReadableDatabase();
        String selectQuery = "select "+dechet+" from Copro";
        Cursor curs = bd.rawQuery(selectQuery, null);

        if (curs.moveToFirst()){
            int coproPapier = curs.getInt(curs.getColumnIndexOrThrow(dechet));
            String resultat = "";
            if (coproPapier == 1){
                resultat = "Jaune";
            }else if (coproPapier == 2){
                resultat = "Bleu";
            }else if (coproPapier == 3){
                resultat = "Verte";
            }
            return resultat;
        }else{
            return "tout venant";
        }
        
    }
    
}
