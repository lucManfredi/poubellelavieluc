package com.example.poubellelavie.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.poubellelavie.R;

import java.util.List;

public class AdapterPersoMain extends BaseAdapter {
    private Context context;
    private List<String> valeurs;
    private List<Integer> images;

    private static LayoutInflater inflater = null;

    public AdapterPersoMain(Context context, List<String> valeurs, List<Integer> images){
        this.context = context;
        this.valeurs = valeurs;
        this.images = images;

        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return valeurs.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View previous = convertView;
        View retour = previous;

        if (retour == null){
            retour = inflater.inflate(R.layout.liste_item_dechet, null);
        }
        TextView text = (TextView) retour.findViewById(R.id.textViewDechet);
        text.setText(valeurs.get(position));
        ImageView image = (ImageView) retour.findViewById(R.id.imageViewDechet);
        image.setImageResource(images.get(position));

        return retour;
    }
}
